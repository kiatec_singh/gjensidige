# Sundhedsunivers

## Prerequisities

To build the project, we have a few workarounds to accommodate.

1. Install the components

   ```
   npm i
   ```

2. Open **node_modules/ngx-toastr/toastr-bs4-alert.scss** and remove the whole section from **line 55**

## Android

### Build for UAT

1. Build the application (the command might fail at first. Re-running it fixes the issue)

   ```
   cross-env ENV='uat' ionic cordova build android --prod --release
   ```

2. Sign the application

   ```
   npm run sign:android
   ```

3. Zipalign the application

   ```
   npm run zipalign:android
   ```

4. Deploy the application to **internal test**, at [Play Store](https://play.google.com/console/u/1/developers/7334574299952483262/app/4973943003359776635/app-dashboard?timespan=thirtyDays&showKpiMenu=null)

<br>

### Build for PROD

1. Build the application

   ```
   cross-env ENV='prod' ionic cordova build android --prod --release
   ```

2. Sign the application

   ```
   npm run sign:android
   ```

3. Zipalign the application

   ```
   npm run zipalign:android
   ```

4. Deploy the application to **production** at [Play Store](https://play.google.com/console/u/1/developers/7334574299952483262/app/4973943003359776635/app-dashboard?timespan=thirtyDays&showKpiMenu=null)
