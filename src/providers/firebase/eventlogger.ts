import { Injectable } from '@angular/core';
import { AnalyticsFirebase } from '@ionic-native/analytics-firebase';
import { AuthService } from '../authenticationservice/auth.service';
import { Platform } from 'ionic-angular';
import { BaseRestService } from '../restservice/base.rest.service';
import { Location } from '@angular/common';
// import { GoogleAnalytics } from '@ionic-native/google-analytics';

declare let document: any;



@Injectable()
export class EventLoggerProvider {

    public userinfo: any;
    public agreed: any;
    public hostname: any;
    public autorisation: any;
    constructor(public fba: AnalyticsFirebase, public auth: AuthService, private platform: Platform, private location: Location, private baserestSerivce: BaseRestService) {
        this.auth.user.subscribe(
            (userinfo) => {
                    this.userinfo = userinfo;
            }
        );
        this.auth.agreed.subscribe(
            (agreed) => {
                    this.agreed = agreed;
            }
        );
        this.auth.autorisation.subscribe(
            (autorisation) => { this.autorisation = autorisation }
        );
    }
    logEvent(eventname: string) {
        let loc: any = this.location
        this.hostname = loc._platformStrategy._platformLocation.location.href;
        let platform = 'unkown';
        if (this.hostname.indexOf('carex') !== -1) {
            platform = 'browser'
        }
        else {
            platform = this.platform.is('ios') ? 'ios' : 'android';
        }
        this.baserestSerivce.logAnalytics(this.userinfo&&this.userinfo.brugernavn?this.userinfo.brugernavn:null, platform, this.autorisation, this.userinfo && this.userinfo.id?this.userinfo.id:null, eventname);
    }
    logyoutubeEvent(eventname: string, url) {
        let loc: any = this.location
        this.hostname = loc._platformStrategy._platformLocation.location.href;
        let platform = 'unkown';

        if (this.hostname.indexOf('carex') !== -1) {
            platform = 'browser'
        }
        else {
            platform = this.platform.is('ios') ? 'ios' : 'android';
        }
        this.baserestSerivce.logyoutubeAnalytics(this.userinfo.brugernavn, platform, this.autorisation, this.userinfo.id, eventname, url);

    }
    logCatalogEvent(eventname: string, item) {
        let loc: any = this.location
        this.hostname = loc._platformStrategy._platformLocation.location.href;
        let platform = 'unkown';

        if (this.hostname.indexOf('carex') !== -1) {
            platform = 'browser'
        }
        else {
            platform = this.platform.is('ios') ? 'ios' : 'android';
        }
        this.baserestSerivce.logyoutubeAnalytics(this.userinfo.brugernavn, platform, this.autorisation, this.userinfo.id, eventname, item);
    }
    logpageView(eventname: string) {
        let loc: any = this.location
        this.hostname = loc._platformStrategy._platformLocation.location.href;
        let platform = 'unkown';
        if (this.hostname.indexOf('carex') !== -1) {
            platform = 'browser'
        }
        else {
            platform = this.platform.is('ios') ? 'ios' : 'android';
        }
        this.baserestSerivce.logpageviewAnalytics(this.userinfo.brugernavn, platform, this.autorisation, this.userinfo.id, eventname);
    }
}