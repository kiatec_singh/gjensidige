import { Injectable, Inject } from '@angular/core';
import { Appinfo } from './appinfo';
import { Location, DOCUMENT } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { environment } from '../../environments/environment';

@Injectable()
export class ApplicationLoader {

    public hostname: any;
    public applicationuuid;
    public organization;
    public appversion;
    public branduuid;

    constructor(private location: Location, private appinfo: Appinfo, private titleService: Title, @Inject(DOCUMENT) private _document: HTMLDocument) {

    }

    getWebappConfig() {
        let loc: any = this.location
        this.hostname = loc._platformStrategy._platformLocation.location.href;
        if (this.hostname.indexOf('molholmsundhedsunivers') !== -1) {
            this.molholm();
        }
        else if (this.hostname.indexOf('trygsundhed') !== -1) {
            this.tryg();
        }
        else if (this.hostname.indexOf('sundhedviaap') !== -1) {
            this.ap();
        }
        else if (this.hostname.indexOf('connect') !== -1) {
            this.carexconnect();
        }
        else if (this.hostname.indexOf('sundhedsunivers') !== -1) {
            this.gjensidige();
        }
        else if (this.hostname.indexOf('workplace') !== -1) {
            this.workplace();
        }
        else {
            this.gjensidige();
        }
    }

    tryg() {
        this.appversion = environment.version;
        this.appinfo.setAppversion(environment.version);
        this.applicationuuid = 'ae9055e5-210e-4a0b-9d39-b334c12cf744';
        this.organization = '7fb031ae-0cb9-4a04-8407-1156d3d52108';
        this.branduuid = '59aa2f87-d09e-4fd7-9837-d3342082b35a';
        this.appinfo.setApplicationinfo(this.organization, this.applicationuuid,this.branduuid);
        if (this.hostname.indexOf('trygsundhed') !== -1) {
            this.titleService.setTitle('Tryg Sundhed');
            if (this._document.getElementById('appFavicon')) {
                this._document.getElementById('appFavicon').setAttribute('href', 'assets/icon/tryg-fav-icon.png');
            }
        }

    }
    molholm() {
        this.appversion = environment.version;
        this.appinfo.setAppversion(environment.version);
        this.applicationuuid = 'dc252eac-7759-4de1-a371-25da7a76761b';
        this.organization = 'bef7e721-29cb-4eec-b0a9-af70fdc7b05d';
        this.branduuid = '502583b1-4afc-4201-87de-5a2cc287e837';
        this.appinfo.setApplicationinfo(this.organization, this.applicationuuid,this.branduuid);
        if (this.hostname.indexOf('molholmsundhedsunivers') !== -1) {
            this.titleService.setTitle('Molholmsundhedsunivers');
            if (this._document.getElementById('appFavicon')) {
                this._document.getElementById('appFavicon').setAttribute('href', 'assets/icon/molholm-fav-icon.png');
            }

        }
    }
    ap() {
        this.appversion = environment.version;
        this.appinfo.setAppversion(environment.version);
        this.applicationuuid = '86d383ea-644c-49df-9d8c-0fdde0811a35';
        this.organization = '8e7fcffe-444b-4468-ab59-9f63b00d15c5';
        this.branduuid = 'b9701609-42a2-47c5-9350-a654cd342af8';
        this.appinfo.setApplicationinfo(this.organization, this.applicationuuid,this.branduuid);
        if (this.hostname.indexOf('sundhedviaap') !== -1) {
            this.titleService.setTitle('sundhedviaap');
            if (this._document.getElementById('appFavicon')) {
                this._document.getElementById('appFavicon').setAttribute('href', 'assets/icon/ap-fav-icon.png');
            }
        }
    }
    carexconnect() {
        this.appversion = environment.version;
        this.appinfo.setAppversion(environment.version);
        this.applicationuuid = '6966bf6e-d334-41a5-b5ee-99e7d015118f';
        this.organization = '59aa2f87-d09e-4fd7-9837-d3342082b35a';
        this.branduuid = 'a2e1d722-edd1-41fd-9196-5429662f96ad';
        this.appinfo.setApplicationinfo(this.organization, this.applicationuuid,this.branduuid);
        if (this.hostname.indexOf('connect') !== -1) {
            this.titleService.setTitle('Carex Connect');
            if (this._document.getElementById('appFavicon')) {
                this._document.getElementById('appFavicon').setAttribute('href', 'assets/icon/connect-fav-icon.png');
            }
        }
    }
    workplace() {
        this.appversion = environment.version;
        this.appinfo.setAppversion(environment.version);
        this.applicationuuid = '6966bf6e-d334-41a5-b5ee-99e7d015118f';
        this.organization = '59aa2f87-d09e-4fd7-9837-d3342082b35a';
        this.branduuid = 'a2e1d722-edd1-41fd-9196-5429662f96ad';
        this.appinfo.setApplicationinfo(this.organization, this.applicationuuid,this.branduuid);
        if (this.hostname.indexOf('workplace') !== -1) {
            this.titleService.setTitle('Carex Workplace');
            if (this._document.getElementById('appFavicon')) {
                this._document.getElementById('appFavicon').setAttribute('href', 'assets/icon/connect-fav-icon.png');
            }
        }
    }
    gjensidige(){
        this.appversion = environment.version;
        this.appinfo.setAppversion(environment.version);
        this.applicationuuid = 'e1bdaed0-9f7a-4288-ac95-a6b00c41857a';
        this.organization = '38507c00-97bf-43f1-9c76-1d2f1159de31';
        this.branduuid = '91bb87cd-b1f9-421e-aaf6-49a73cea058e';//default branduuid   //91bb87cd-b1f9-421e-aaf6-49a73cea058e
        this.appinfo.setApplicationinfo(this.organization, this.applicationuuid,this.branduuid);
        if (this.hostname.indexOf('sundhedsunivers') !== -1) {
            this.titleService.setTitle('Sundhedsunivers');
            if (this._document.getElementById('appFavicon')) {
                this._document.getElementById('appFavicon').setAttribute('href', 'assets/icon/sundhedsunivers-fav-icon.png');
            }
        }
    }
}