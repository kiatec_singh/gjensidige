import { Injectable, ViewChild } from '@angular/core';;
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { Platform, App } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { ToastrService } from 'ngx-toastr';
import { AlldetailsPage } from '../../pages/alldetails/alldetails.page';
import { ListdetailsPage } from '../../pages/listdetails/listdetails.page';
import { SmartsearchPage } from '../../pages/smartsearch/smartsearch.page';
import { ProfilePage } from '../../pages/profile/profile.page';
import { HelperSerice } from './helperService';
import { questionairehistoryPage } from '../../pages/questionairehistory/questionairehistory.page';
import { Appinfo } from './appinfo';
import { QuestionairePage } from '../../pages/questionaire/questionaire.page';
import { Events } from 'ionic-angular/util/events';
import { ChangelanguageService } from './changelanguageservice';

@Injectable()
export class deeplinkingService {
  private url;
  private urltext;
  private isdownload = false;
  private isdelete = false;
  private userdata;
  private isvitaltest = false;
  private appconfig: any;
  // private isLoading = false;
  private autorisation;
  private resultpath;
  private tabs;
  private anchorContent;
  private slctdItem: any;
  private detailsdata: any;
  private tabsgroup;
  private nav: any;
  public selectedlanguage

  constructor(private app: App,private lang:ChangelanguageService,
    private helperService: HelperSerice, private auth: AuthService,private events:Events,
     private appinfo: Appinfo, private toastr: ToastrService, private platform: Platform, 
     private baserestService: BaseRestService, private toastCtrl: Toast) {
    this.auth.autorisation.subscribe(
      (autorisation) => {
        this.autorisation = autorisation;
      }
    );
  //   this.auth.appconfig.subscribe(
  //     (appconfig) => {
  //         this.appconfig = appconfig;
  //         if (appconfig) {
  //             this.anchorContent = appconfig.configuration.texts.da.anchor;
  //         }
  //     }
  // );
  this.lang.selectedlanguage.subscribe(
    (selectedlanguage: any) => {
      if(selectedlanguage){
            this.selectedlanguage = selectedlanguage;
            this.anchorContent = selectedlanguage.anchor;
        }}
   );
    this.nav = this.app.getRootNav();
  }
  navigateinAPP(functionname) {
    if(functionname ==='profil'){
      this.nav.setRoot(ProfilePage);
      this.nav.popToRoot();
    }else{
         this.baserestService.getnavigationPath(this.autorisation, functionname).then(
      (resultpath) => {
        this.resultpath = resultpath; this.decidenavigationPath(resultpath);
      },
      error => {
        this.errorblock();
      }); 
    }
  }

  decidenavigationPath(resultdata) {
    this.slctdItem = resultdata;
    console.log(resultdata);
    if (!resultdata) {
      this.errorblock();
      this.auth.setSpinner(false);
      // if (!this.ref['destroyed']) {
      //   this.ref.detectChanges();
      // }
    } else {
      this.navigate();
    }

  }
  checkauthforNav(resultpathdata) {
    this.slctdItem = resultpathdata;
    // this.navigate();
    // this.baserestService.checkauthforNav(this.autorisation, resultpath.knap.click_action, resultpath.knap.skema_uuid).then(
    //     (resultdata) => { this.navigate(resultdata); },
    //     error => {
    //         isLoading = false;

    //     }
    // );
  }
  navigate() {
    let actionparameterkey = '';
    let actionparameterkeyId = '';
     if (this.slctdItem.knap.click_action === "ReadSurveyAndAnswers") {
      actionparameterkey = 'survey_uuid';
      this.appinfo.setSlctdItem(this.slctdItem.knap);
      let aktivitet_uuid  = this.slctdItem.aktivitet_uuid? this.slctdItem.aktivitet_uuid: '';
      this.baserestService.getquestionairdetails(this.slctdItem.knap.click_action, actionparameterkey, this.slctdItem.knap.click_action_parameters.survey_uuid, aktivitet_uuid,this.autorisation).then(
        allquestionsdata => {
          this.appinfo.setQuestionaireData(allquestionsdata);
          this.auth.setSpinner(false);
          this.events.publish('spinner',false);
          this.nav.push(QuestionairePage, { 'pagetitle': this.slctdItem.knap.funktionsnavn, 'heading': '','fromnotification': true });
        },
        error => {    this.auth.setSpinner(false);this.auth.setSpinner(false);this.errorblock(); }
      );
    }
  }
  showdetailsPage(tabstoDisplay) {
    this.auth.setSpinner(false);
    this.auth.setdetailContent(this.tabsgroup.indhold.indhold);
    this.nav.push(AlldetailsPage, { 'tabstoDisplay': tabstoDisplay, 'pagetitle': this.slctdItem.knap.funktionsnavn });
  }
  naviDetails() {
    let tabsContent = [];
    let tabstoDisplay = [];
    if (this.slctdItem.click_action) {
      if (this.detailsdata && this.detailsdata.indhold) {
        this.auth.setSpinner(false);
        tabstoDisplay.push(this.detailsdata.indhold.tab_titel)
      }
      if (this.detailsdata && this.detailsdata.group_tabs) {
        this.auth.setSpinner(false);
        tabstoDisplay.push(this.detailsdata.indhold.tab_titel)
      }
      if (this.slctdItem.click_action === "ReadSurveyAndAnswers") {
        this.auth.setSpinner(false);
        //  this.nav.push(SmartsearchPage, { 'pagetitle': 'Smart søg', 'smartsearchData': this.detailsdata });
        this.nav.push(questionairehistoryPage, { 'data': this.detailsdata });
      }
      if (this.slctdItem.click_action === "objektListSmartSearch") {
        this.auth.setSpinner(false);
        this.nav.push(SmartsearchPage, { 'pagetitle': 'Smart søg', 'smartsearchData': this.detailsdata });
      }
      else {
        this.auth.setSpinner(false);
        if (this.detailsdata && this.detailsdata.indhold && this.detailsdata.indhold.indhold) {
          this.auth.setdetailContent(this.detailsdata.indhold.indhold);
          this.nav.push(AlldetailsPage, { 'tabstoDisplay': tabstoDisplay, 'pagetitle': this.slctdItem.funktionsnavn });

        }
      }
    }
    //  this.nav.push(AlldetailsPage, { 'tabsContent': tabsContent, 'tabstoDisplay': tabstoDisplay, pagedata, 'pagetitle': slctdItem[1] });
  }
  successblock() {
    this.auth.setSpinner(false);
    let isApp = this.helperService.isApp();
    if (isApp === true) {
      const toast = this.toastCtrl.show(this.anchorContent.submitsuccessmsg, this.anchorContent.toastdration, this.anchorContent.toastposition).subscribe(
        toast => {
          console.log(toast);
          // this.viewCtrl.dismiss();
        });
    }
    else {
      this.toastr.success(this.anchorContent.submitsuccessmsg, this.anchorContent.submitsuccesstitle, {
        timeOut: this.anchorContent.toastdration,
        positionClass: this.anchorContent.webtoastposition
      });

    }

  }
  errorblock() {
    let isApp = this.helperService.isApp();
    this.auth.setSpinner(false);
    this.events.publish('spinner',false);
    if (isApp === true) {
      const toast = this.toastCtrl.show(this.anchorContent.accessdenied, this.anchorContent.toastdration, this.anchorContent.toastposition).subscribe(
        toast => {
          console.log(toast);
          //  this.viewCtrl.dismiss();
        });
    }
    else {
      this.toastr.error(this.anchorContent.accessdenied, this.anchorContent.submiterrortitle, {
        timeOut: this.anchorContent.toastdration,
        positionClass: this.anchorContent.webtoastposition
      });

    }

  }

}

