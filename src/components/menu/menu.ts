import { Component, ViewChild } from '@angular/core';
import { Nav, NavController, MenuController, ModalController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home.page';
import { SettingsPage } from '../../pages/settings/settings.page';
import { ProfilePage } from '../../pages/profile/profile.page';
import { NotificationsPage } from '../../pages/notifications/notifications.page';
import { TermsconditionPage } from '../../pages/termsconditions/termsconditions.page';
import { OtherRelationsPage } from '../../pages/otherrelations/otherrelations.page';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { StorageService } from '../../providers/storageservice/storageservice';
import { CPRPage } from '../../pages/cpr/cpr.page';
import { IdverifyPage } from '../../pages/idverify/idverify.page';
import { StatusBar } from '@ionic-native/status-bar';
import { ThemeChangeService } from '../../providers/restservice/themechange.service';
import { ConfigurePage } from '../../pages/configure/configure.page';
import { ApplicationLoader } from '../../providers/utils/applicationloader';
import { Appinfo } from '../../providers/utils/appinfo';
import { ChangelanguagePage } from '../../pages/changelanguage/changelanguage.page';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';
import { changelanguageComponent } from '../changelanguage/changelanguage';


@Component({
  selector: 'menu-viewer',
  templateUrl: 'menu.html'
})
export class MenuComponent {
  @ViewChild(Nav) nav: Nav;

  menuList: any;
  rootPage: any;
  private menuItems: any;
  private loading: boolean;
  private environment;
  private userinfo;
  private appversion;
  private cprsaved;
  private userchecklistdata;
  private appconfig;
  private appName;
  private themeid: any;
  private selectedlanguage;

  pages: Array<{ title: string, component: any }>;
  constructor(private baserestService: BaseRestService,
     public apploader: ApplicationLoader, private lang:ChangelanguageService,
     private appinfo: Appinfo, public menuCtrl: MenuController,
    private statusbar: StatusBar,
    private modalCtrl: ModalController,
    private storageService: StorageService, private themechangeService: ThemeChangeService, private auth: AuthService) {
    this.auth.userchecklistdata.subscribe(
      (userchecklistdata) => {
          this.userchecklistdata = userchecklistdata;
      }
    );
    this.auth.user.subscribe(
      (userinfo) => {
          this.userinfo = userinfo;
      }
    );
    this.auth.appconfig.subscribe(
      (appconfig: any) => {
          this.appconfig = appconfig;
      }
    );
    this.lang.selectedlanguage.subscribe(
      (selectedlanguage) => {
        if(selectedlanguage){
          this.selectedlanguage = selectedlanguage;
          this.menuItems = selectedlanguage.menu;
        }
        }
    );

  }

  ngOnInit() {
    console.log("in menu");
    this.apploader.getWebappConfig();
    let check = this.appinfo.getApplicationinfo();
    this.auth.setShowback(false);
    this.storageService.themeid.then((themeid) => {
      this.themeid = themeid;
      if (this.themeid) {
        this.baserestService.getbrandConfigbyId(this.themeid).then(
          (appconfig: any) => {
            this.appconfig = appconfig;
            this.setappConfig();
           });
      }
      else if (!this.themeid) {
        this.baserestService.getappConfig().then(
          (appconfig: any) => {
            this.appconfig = appconfig;
            this.setappConfig();
 
          });

      }
    });
  }
 setappConfig(){
   console.log("in menu setappcongig");
  this.statusbar.backgroundColorByHexString(this.appconfig.configuration.primary);
  if (this.appconfig.configuration.statusbartext == 'light') {
    this.statusbar.styleLightContent();
  }
  if (this.appconfig.configuration.statusbartext == 'dark') {
    this.statusbar.styleDefault();
  }
  this.auth.setappconfig(this.appconfig)
  this.themechangeService.changetoDynamicTheme(this.appconfig);

  this.storageService.user.then((user) => {
    this.userinfo = user;
    this.auth.setUserinfo(user);
    this.checklistData();
    this.getusermenu();
  });
  this.storageService.selectedlanguage.then((selectedlanguage) => {
    this.selectedlanguage = selectedlanguage;
    this.lang.setselectedlanguge(selectedlanguage);
    this.lang.setuserlang(selectedlanguage);
  });
  
 }
getusermenu(){
  if (this.userinfo && this.userinfo.id && this.appconfig) {
    this.baserestService.getCustomerData(this.userinfo.id).then(
      menuList => { this.menuList = menuList; this.setData(); this.loading = false },
      error => { this.loading = false }
    )
  }
  else{
    this.nav.setRoot(ChangelanguagePage);
  }
}

  setData() {
    this.auth.setEnvironment('http://connect.carex.dk/');
    let pagesarray = [];
    if (this.menuList) {
      this.appName = this.menuList.organisation;
      pagesarray.push({
        title: this.menuList.organisation,
        component: HomePage,
        pageData: this.menuList.result
      });
      this.pages = pagesarray;
    }

    this.loading = false
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    let item = page.pageData;
    let itemIndex = page.index;
    this.nav.setRoot(page.component, { 'pageData': item, 'itemIndex': itemIndex });
    this.nav.popToRoot();
  }
  checklistData(){
    if(this.userinfo && this.userinfo.id){
        this.baserestService.checkactiveList(this.userinfo.id).then(
      checklistdata => {
          this.storageService.set('checklistdata', checklistdata);
          this.auth.setuserchecklistData(checklistdata);
          this.getselectedlanguage();
          this.userchecklistdata = checklistdata;
      },
      error => {
          console.log(error);
      }
  )
    }else{
      this.nav.setRoot(ChangelanguagePage);
    }
  

  }
  getselectedlanguage(){
    this.storageService.get('selectedlanguage').then(
      selectedlanguage => {
        if(selectedlanguage){
          this.selectedlanguage = selectedlanguage;
          this.lang.setselectedlanguge(selectedlanguage);
          this.lang.setuserlang(selectedlanguage);
        }
        else{
            const modal = this.modalCtrl.create(changelanguageComponent, { modal: true });
            modal.present();
        }
        this.decideFlow();
      });
  }
  decideFlow() {
    console.log("menu decide flow");
      if (this.userchecklistdata) {
        if (!this.userchecklistdata.result || this.userchecklistdata.result.length == 0) {
          this.nav.setRoot(ChangelanguagePage);
          return;
        }
        if (this.userchecklistdata && this.userchecklistdata.result && this.userchecklistdata.result.generalaccept === true && this.userchecklistdata.result.cpr === true && this.userchecklistdata.result.nemid) {
          this.nav.setRoot(HomePage);
          return;
        }
        if (this.userchecklistdata && this.userchecklistdata.result && !this.userchecklistdata.result.generalaccept && !this.userchecklistdata.result.cpr && !this.userchecklistdata.result.nemid) {
          this.nav.setRoot(ChangelanguagePage);
          return;
        }
        if (this.userchecklistdata && this.userchecklistdata.result && this.userchecklistdata.result.generalaccept === true && this.userchecklistdata.result.cpr === false && !this.userchecklistdata.result.nemid) {
          this.nav.setRoot(IdverifyPage);
          return;
        }

        if (this.userchecklistdata && this.userchecklistdata.result && !this.userchecklistdata.result.generalaccept && this.userchecklistdata.result.cpr === false && this.userchecklistdata.result.nemid) {
          this.nav.setRoot(CPRPage);
          return;
        }
        if (this.userchecklistdata && this.userchecklistdata.result && this.userchecklistdata.result.generalaccept && this.userchecklistdata.result.cpr === false && this.userchecklistdata.result.nemid) {
          this.nav.setRoot(CPRPage);
          return;
        }
        if (this.userchecklistdata && this.userchecklistdata.result && !this.userchecklistdata.result.generalaccept && this.userchecklistdata.result.cpr === false && !this.userchecklistdata.result.nemid) {
          this.nav.setRoot(IdverifyPage);
          return;
        }
        if (this.userchecklistdata && this.userchecklistdata.result && !this.userchecklistdata.result.generalaccept && this.userchecklistdata.result.cpr === true && this.userchecklistdata.result.nemid) {
          this.nav.setRoot(TermsconditionPage);
          return;
        }
        if (this.userchecklistdata && this.userchecklistdata.result && !this.userchecklistdata.result.generalaccept && this.userchecklistdata.result.cpr === false && this.userchecklistdata.result.nemid) {
          this.nav.setRoot(TermsconditionPage);
          return;
        }
      }
      if (!this.userchecklistdata) {
        this.nav.setRoot(ChangelanguagePage);
      }
  }
  openProfile() {
    if (!this.userinfo) {
      this.userinfo = this.auth.getUserInfo();
    }
    this.nav.setRoot(ProfilePage, { 'userinfo': this.userinfo });
    this.nav.popToRoot();
  }
  openNotifications() {
    this.nav.setRoot(NotificationsPage, { 'resultData': '' });
    this.nav.popToRoot();
  }
  openSettings() {
    this.nav.setRoot(SettingsPage, {});
    this.nav.popToRoot();
  }
  openTermsandCondition() {
    this.nav.setRoot(TermsconditionPage, { 'menupage': true });
    this.nav.popToRoot();
  }
  gotoPage() {
    this.nav.setRoot(OtherRelationsPage, {});
    this.nav.popToRoot();
  }
  openConfigpage() {
    this.nav.setRoot(ConfigurePage, {});
    this.nav.popToRoot();
  }
  closeMenu() {
    this.menuCtrl.close();
  }
}
