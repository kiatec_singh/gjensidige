import { Component, ChangeDetectorRef } from '@angular/core';
import { NavParams, NavController } from 'ionic-angular';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { StatusBar } from '@ionic-native/status-bar';
import { MenugeneratorService } from '../../providers/utils/menugenerator';
import { Appinfo } from '../../providers/utils/appinfo';
import { HelperSerice } from '../../providers/utils/helperService';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';




@Component({
  selector: 'navigationbar-viewer',
  templateUrl: 'navigationbar.html'
})
export class navigationbarComponent {

  private userinfo;
  private navbarlist;
  private pagemenuButtons: any;
  public autorisation: any;
  private backtext;



  constructor(public navCtrl: NavController, public pogo: Appinfo, private lang: ChangelanguageService,
    public statusbar: StatusBar, public auth: AuthService, private helperService: HelperSerice, public navParams: NavParams, private baserestService: BaseRestService) {
    this.auth.user.subscribe(
      (userinfo) => {
          this.userinfo = userinfo;
      }
    );
    this.auth.pagemenuButtons.subscribe(
      (pagemenuButtons) => {
          this.pagemenuButtons = pagemenuButtons;
      }
    );
    this.auth.autorisation.subscribe(
      (autorisation) => { this.autorisation = autorisation }
    );
    this.auth.navbarlist.subscribe(
      (navbarlist) => {
          this.navbarlist = navbarlist;
      }
    );
    this.lang.selectedlanguage.subscribe(
      (selectedlanguage: any) => {
        if (selectedlanguage) {
          this.backtext = selectedlanguage.back.title;
        }
      }
    );

  }

  ngOnInit(): void {
    console.log("in back nav");
  }
  home() {
    this.helperService.home();
  }
  detailsPage(slctItem) {
    this.auth.setwebtabstoDisplay(null);
    let actionparameterkey = '';
    let actionparameterkeyId = '';
    if (slctItem.click_action_parameters && slctItem.click_action_parameters.length) {
      for (let i in slctItem.click_action_parameters[0]) {
        actionparameterkey = i[0];
        actionparameterkeyId = slctItem.click_action_parameters[0][i]
      }
    }

    else if (slctItem.brugervendtnoegle && slctItem.brugervendtnoegle.includes("ledersparring_knap")) {
      let navlist = this.navbarlist;
      navlist.pop();
      this.auth.setnavbarlist(navlist);
      this.auth.setwebcontactContent(null);
      this.auth.setwebstressContent(null);
      if (slctItem.submenu_text) {
        this.auth.setdetailContent(slctItem.submenu_text);
      }
    }
    else if (slctItem.brugervendtnoegle && slctItem.brugervendtnoegle.includes("hjaelp_ved_sygdom_knap")) {
      let navlist = this.navbarlist;
      navlist.pop();
      this.auth.setnavbarlist(navlist);
      this.auth.setwebcontactContent(null);
      this.auth.setwebstressContent(null);
      if (slctItem.submenu_text) {
        this.auth.setdetailContent(slctItem.submenu_text);
      }
    }
    else if (slctItem.brugervendtnoegle && slctItem.brugervendtnoegle.includes("trivselsbarometer_knap")) {
      let navlist = this.navbarlist;
      navlist.pop();
      this.auth.setnavbarlist(navlist);
      this.auth.setwebstressContent(null);
      this.auth.setwebhistroryquestionarieContent(null);
      this.auth.setwebQuestionaireContent(null);
      if (slctItem.submenu_text) {
        this.auth.setdetailContent(slctItem.submenu_text);
      }
    }
    else if (slctItem.click_action && slctItem.click_action.includes("objektListSmartSearch")) {
      let searchedLits = this.pogo.getSearchedList();
      if (searchedLits) {
        this.auth.setwebsearchdtlContent(null);
      }
      else {
        this.baserestService.getdetails(slctItem.click_action, slctItem.skema_uuid, this.autorisation).then(
          detailsdata => {
            this.auth.setwebsmartsearchContent(detailsdata);
          },
          error => { console.log(error) }
        )
        this.auth.setwebsearchdtlContent(null);
      }
      this.auth.setpagemenuButtons(this.pagemenuButtons);
      let navlist = this.navbarlist;
      navlist.pop();
      this.auth.setnavbarlist(navlist);
    }
    else if (slctItem.submenu_text) {
      if (slctItem.submenu) {
        this.auth.setpagemenuButtons(slctItem.submenu);
        let newnavbarlist: any;
        for (var j = 0; j < this.navbarlist.length; j++) {
          if ((slctItem.funktionsnavn == this.navbarlist[j].funktionsnavn) && (slctItem.click_action === this.navbarlist[j].click_action)) {
            newnavbarlist = this.navbarlist.slice(0, j + 1);
          }
        }
        this.auth.setnavbarlist(newnavbarlist);
        this.auth.setwebtabsContent(slctItem.submenu_text);
        this.auth.setdetailContent(slctItem.submenu_text);
        this.navbarlist = newnavbarlist;
      }
      else {
        this.auth.setwebtabsContent(slctItem.submenu_text);
        this.auth.setdetailContent(slctItem.submenu_text);
        let navlist = this.navbarlist;
        navlist.pop();
        this.auth.setnavbarlist(navlist);
      }

    }

    else {
      this.auth.setpagemenuButtons(slctItem.submenu);
      let newnavbarlist: any;
      for (var i = 0; i < this.navbarlist.length; i++) {
        if ((slctItem.funktionsnavn == this.navbarlist[i].funktionsnavn) && (slctItem.click_action === this.navbarlist[i].click_action)) {
          newnavbarlist = this.navbarlist.slice(0, i + 1);
        }
      }
      this.auth.setnavbarlist(newnavbarlist);
      this.navbarlist = newnavbarlist;
    }
  }

}
