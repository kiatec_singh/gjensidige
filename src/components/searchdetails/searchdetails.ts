import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { Platform } from 'ionic-angular/platform/platform';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { environment } from '../../environments/environment';

@Component({
    selector: 'searchdetails-viewer',
    templateUrl: 'searchdetails.html'
})
export class SearchDetailsComponent {

    private searchselectedItem;
    private heading;
    private image;
    private teaser;
    private detailsparagraphs;
    private pagetitle: any;
    private bilagurl;
    private bilagName;
    private iframedata = [];
    public slctsearchItem: any;
    private billedeUrl: any;
    private appconfig: any;
    private searchdetatilsData: any;

    constructor(private iab: InAppBrowser, private navParam: NavParams, private auth: AuthService) {
        this.searchselectedItem = this.navParam.get('selectedItem');
        this.pagetitle = this.navParam.get('pagetitle');
        if (!this.searchselectedItem) {
            this.auth.websearchdtlContent.subscribe(
                (slctsearchItem) => {
                    this.searchselectedItem = slctsearchItem;
                    if (this.slctsearchItem && this.slctsearchItem.status) {
                        this.setData();
                    }
                }
            );
        }
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                    this.appconfig = appconfig;
            }
        );

    }

    ngOnInit() {
        this.setData();
    }
    setData() {
        if (this.searchselectedItem && this.searchselectedItem.titel) {
            this.heading = this.searchselectedItem.titel;
            this.teaser = this.searchselectedItem.teaser;
            this.detailsparagraphs = this.searchselectedItem.skabelonfiletext;
            if (this.searchselectedItem.bilag) {
                this.bilagurl = this.appconfig.environment.bilagUrl + this.searchselectedItem.bilag;
                this.bilagName = this.searchselectedItem.bilag;
            } if (this.searchselectedItem.billede) {
                this.image = this.searchselectedItem.billede;
                this.billedeUrl = this.appconfig.environment.picturesUrl + this.searchselectedItem.billede;
            }
            let totaliframes = [];
            this.detailsparagraphs.forEach(element => {
                if (element.indexOf('<iframe') != -1) {
                    totaliframes.push(element);
                }
            });
            this.iframedata = totaliframes;
        }

    }

    downloadPdf(event) {
        event.preventDefault();
        this.iab.create("https://docs.google.com/viewer?url=" + this.bilagurl, "_system", "location=no,hardwareback=yes");
    }

}


