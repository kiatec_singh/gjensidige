import { Component, Input, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { EventLoggerProvider } from '../../providers/firebase/eventlogger';
import Player from '@vimeo/player';

@Component({
    selector: 'vimeo-viewer',
    templateUrl: 'vimeo.html',
})

export class VimeoComponent {
    @Input() videoUrl: any;
    @Input() videoId: any;
    private appconfig: any;
    @ViewChild('vimeoiframe') vimeoiframe: ElementRef;
    constructor(private logprovider: EventLoggerProvider, public auth: AuthService) {
        this.auth.appconfig.subscribe(
            (appconfig) => {
                this.appconfig = appconfig;
            }
        )
    }
    ngOnInit() {
        console.log(this);
    }
    ngAfterViewInit(): void {
       this.videoId = +this.videoId;
        var options = {
            id: this.videoId,
            width: 640,
            height: 280,
            loop: true
        };

         var player = new Player(this.vimeoiframe.nativeElement);
        let newthis: any = this;
        player.on('play', function () {
            if (newthis.appconfig.environment.trackanalytics === true) {
                newthis.logprovider.logyoutubeEvent('vimeo', newthis.videoUrl);
            }
        }, this);

    }
    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.videoId = null;
    }

}