
import { Component, HostListener, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CPRPage } from '../../pages/cpr/cpr.page';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { StorageService } from '../../providers/storageservice/storageservice';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';

window;

@Component({
    selector: 'nemid-viewer',
    templateUrl: 'nemid.html'
})
export class NemidComponent {

    private usernemid;
    private nemiddata;
    private heading;
    private iframenemidUrl;
    private iframeurl = '';
    private appconfig;
    private selectedlanguage;
    private serviceerror;
    @ViewChild('iframe') iframe: any;

    constructor(private navCtrl: NavController,
        private lang: ChangelanguageService, private ref: ChangeDetectorRef,
        private storage: StorageService, private auth: AuthService) {
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                this.appconfig = appconfig;
                if (appconfig && appconfig.environment) {
                    this.iframenemidUrl = appconfig.environment.nemidloginUrl;
                }
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                if (selectedlanguage) {
                    this.selectedlanguage = selectedlanguage;
                    this.nemiddata = selectedlanguage.nemid;
                }
            }
        );
    }

    ngOnInit() {
        this.setData();
    }
    setData() {
        this.heading = this.nemiddata.heading;
        console.log(this);
    }

    @HostListener('window:message', ['$event'])
    onMessage(e) {
        if (e.data && e.data.AuthenticationInfo) {
            this.usernemid = e.data;
            this.auth.setUsernemiddata(e.data.AuthenticationInfo);
            this.storage.set('userpiddata', e.data.AuthenticationInfo);
            this.navCtrl.setRoot(CPRPage, { 'userpiddata': e.data.AuthenticationInfo });
        }
        else {
            this.serviceerror = e.data ? e.data : this.nemiddata.errormsg;
            this.iframe.nativeElement.src = this.appconfig.environment.nemidloginUrl;
            this.ref.detectChanges();
        }
    }
}
