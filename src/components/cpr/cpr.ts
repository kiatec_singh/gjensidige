import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { NemidPage } from '../../pages/nemid/nemid.page';
import { TermsconditionPage } from '../../pages/termsconditions/termsconditions.page';
import { PasswordValidation } from '../../providers/validators/password-validator';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { StorageService } from '../../providers/storageservice/storageservice';
import { HomePage } from '../../pages/home/home.page';
import { IdverifyPage } from '../../pages/idverify/idverify.page';
import { Cpr } from '../../models/cpr.model';
import { HelperSerice } from '../../providers/utils/helperService';
import { Keyboard } from '@ionic-native/keyboard';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';


@Component({
    selector: 'cpr-viewer',
    templateUrl: 'cpr.html'
})
export class CPRComponent {


    private cprForm: FormGroup;
    private iscprvalid;
    private cprinvaliderror;
    private userpiddata;
    private cprsaved;
    private userdata;
    private serviceerror;
    private serviceerrormessage = '';
    private activestatus;
    private userchecklistdata;
    private userpid = '';
    private loader: any;
    private isLoading=false;
    private cprdata: Cpr;
    public appconfig;
    private selectedlanguage;

    constructor(private navCtrl: NavController,
        private ref: ChangeDetectorRef, private helperService: HelperSerice, private keyboard: Keyboard,
        private lang: ChangelanguageService,
        private storageService: StorageService, private navParam: NavParams, private baseservice: BaseRestService, private fb: FormBuilder, private storageservice: StorageService, private auth: AuthService, private baserestService: BaseRestService) {
        this.cprForm = this.fb.group({
            cprnummner: ['', Validators.required],
            yes: [false, Validators.required]
        }, {
            validator: PasswordValidation.cprValidator // your validation method
        });
        this.auth.userchecklistdata.subscribe(
            (userchecklistdata) => {
                if (userchecklistdata && userchecklistdata.result) {
                    this.userchecklistdata = userchecklistdata;
                }
            }
        );
        this.auth.user.subscribe(
            (userdata) => {
                if (userdata && userdata.id) {
                    this.userdata = userdata;
                }
            }
        );
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                if (appconfig && appconfig.configuration) {
                    this.appconfig = appconfig;
                }
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                this.selectedlanguage = selectedlanguage;
            }
        );
    }

    ngOnInit() {
        this.isLoading = true;
        this.userpiddata = this.navParam.get('userpiddata');
        this.getcprData();
    }

    getcprData() {
        if (this.appconfig && this.appconfig.configuration) {
            if (this.selectedlanguage) {
                this.setData(this.selectedlanguage.cpr);
            } else {
                this.setData(this.appconfig.configuration.texts.da.cpr);
            }

        } else {
            this.baseservice.getappConfig().then(
                (appconfig: any) => {
                    this.appconfig = appconfig;
                    if (this.selectedlanguage) {
                        this.setData(this.selectedlanguage.cpr);
                    } else {
                        this.setData(this.appconfig.configuration.texts.da.cpr);
                    }
                    this.auth.setappconfig(appconfig);
                });
        }
    }
    setData(cprdata) {
        this.cprdata = new Cpr(cprdata);
        this.isLoading = false;
        this.ref.detectChanges();
        this.cprForm.valueChanges.subscribe(
            value => { this.ref.detectChanges(); }
        );
        this.cprForm.statusChanges.subscribe(
            value => { this.ref.detectChanges(); }
        );
    }

    cprValidate() {
        this.isLoading = true;
        if (this.userchecklistdata && this.userchecklistdata.result && this.userchecklistdata.result.nemid) {
            this.userpid = this.userchecklistdata.result.nemid;
        }
        if (this.userpiddata && this.userpiddata.Pid) {
            this.userpid = this.userpiddata.Pid;
        }

        let cprvalue = this.cprForm.value.cprnummner;
        this.baserestService.validateCPR(this.userpid, cprvalue).then(
            iscprvalid => {
                this.iscprvalid = iscprvalid;
                if (iscprvalid === null) {
                    this.isLoading = false;
                    this.serviceerror = true;
                    this.cprinvaliderror = true;
                    this.ref.detectChanges();
                }
                else {
                    this.serviceerror = false;
                    this.cprinvaliderror = false;
                    this.saveCPR(cprvalue);
                }
            },
            error => {
                console.log(error);
                this.isLoading = false;
                this.serviceerror = false;
                this.cprinvaliderror = false;
                this.serviceerrormessage = error.error ? error.error : error.statusText;
                this.ref.detectChanges();
            }
        );
    }
    termsAccepted() {
        this.ref.detectChanges();
    }
    gotoNemid() {
        this.navCtrl.setRoot(NemidPage);
    }

    removeErrors() {
        this.serviceerror = false;
        this.cprinvaliderror = false;
    }
    saveCPR(cprvalue) {
        this.activestatus = this.cprForm.value.yes === true ? 'aktiv' : 'inaktiv';
        this.baserestService.saveCPR(this.userpid, cprvalue, this.activestatus, this.userdata.id).then(
            cprsaved => {
                cprsaved = this.cprsaved; this.savecprnavi();
            },
            error => {
                this.isLoading = false;
                this.serviceerror = false;
                this.cprinvaliderror = false;
                console.log(error);
                this.serviceerrormessage = error.error ? error.error : error;
                this.ref.detectChanges();
            }
        )
    }
    showKeyboard() {
        let isapp = this.helperService.isApp();
        if (isapp === true) {
            if (!this.keyboard.isVisible) {
                this.keyboard.show();
                this.ref.detectChanges();
            }
        }
    }
    savecprnavi() {
        this.storageservice.set('cprsave', this.activestatus);
        this.baserestService.checkactiveList(this.userdata.id).then(
            checklistdata => {
                this.storageService.set('checklistdata', checklistdata);
                this.auth.setuserchecklistData(checklistdata);
                this.decideflow(checklistdata);
            },
            error => {
                this.isLoading = false;
                console.log(error);
                this.serviceerror = true;
                this.serviceerrormessage = error.error ? error.error : error;
                this.ref.detectChanges();
            }
        );

    }
    decideflow(checklistdata) {
        this.isLoading = false;
        if (!checklistdata.result || checklistdata.result.length == 0) {
            this.navCtrl.push(IdverifyPage);
            return;
        }
        if (checklistdata && checklistdata.result && !checklistdata.result.generalaccept && !checklistdata.result.cpr && !checklistdata.result.nemid) {
            this.navCtrl.push(IdverifyPage);
            return;
        }
        if (checklistdata && checklistdata.result && !checklistdata.result.generalaccept && checklistdata.result.cpr === true && checklistdata.result.nemid) {
            this.navCtrl.push(TermsconditionPage, { 'menupage': false });
            return;
        }
        if (checklistdata && checklistdata.result && !checklistdata.result.generalaccept && checklistdata.result.cpr === false && checklistdata.result.nemid) {
            this.navCtrl.push(TermsconditionPage, { 'menupage': false });
            return;
        }
        if (checklistdata && checklistdata.result && checklistdata.result.generalaccept === true && checklistdata.result.cpr === true && checklistdata.result.nemid) {
            this.navCtrl.push(HomePage);
            return;
        }
        if (checklistdata && checklistdata.result && checklistdata.result.generalaccept === true && checklistdata.result.cpr === false && checklistdata.result.nemid) {
            this.navCtrl.push(HomePage);
            return;
        }
        if (checklistdata && checklistdata.result && checklistdata.result.generalaccept && !checklistdata.result.cpr) {
            this.navCtrl.setRoot(HomePage);
            return;
        }

    }
}


