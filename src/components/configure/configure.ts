
import { Component } from '@angular/core';
import { AuthService } from '../../providers/authenticationservice/auth.service'


@Component({
    selector: 'configure-viewer',
    templateUrl: 'configure.html'
})


export class ConfigureComponent {
    private appconfig: any;
    private configarray=[];
    constructor(private auth: AuthService) {
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                {
                    this.appconfig = appconfig;
                    this.configarray = [appconfig];
                }
            }
        );
    }

    ngOnInit() {
    }

}
