import { Component, Input, ChangeDetectorRef, NgZone } from '@angular/core';
import { AuthService } from '../../providers/authenticationservice/auth.service';

// var htmlAttributeParser = require('html-attribute-parser');
// const ngHtmlParser = require('angular-html-parser');
 var parse = require('node-html-parser');
// import { parse } from 'node-html-parser';
// const parse5 = require('parse5');
// var parse5 = require('fast-html-parser');
// var HTML = require('html-parse-stringify')
@Component({
    selector: 'htmlcontent-viewer',
    templateUrl: 'htmlcontent.html'
})
export class HtmlcontentComponent {
    @Input() htmlcontent: any;
    public htmlelements=[];
    public commontags=[
        'ul',
        'ol',
        'abbr',
        'address',
        'area',
        'article',
        'aside',
        'audio',
        'b',
        'bdi',
        'bdo',
        'blockquote',
        'body',
        'br',
        'button',
        'canvas',
        'caption',
        'cite',
        'code',
        'col',
        'colgroup',
        'command',
        'datalist',
        'dd',
        'del',
        'details',
        'dfn',
        'div',
        'dl',
        'dt',
        'em',
        'embed',
        'fieldset',
        'figcaption',
        'figure',
        'footer',
        'form',
        'h1',
        'h2',
        'h3',
        'h4',
        'h5',
        'h6',
        'header',
        'hr',
        'html',
        'i',
        'iframe',
        'img',
        'input',
        'ins',
        'kbd',
        'keygen',
        'label',
        'legend',
        'li',
        'main',
        'map',
        'mark',
        'menu',
        'meter',
        'nav',
        'object',
        'ol',
        'optgroup',
        'option',
        'output',
        'p',
        'param',
        'pre',
        'progress',
        'q',
        'rp',
        'rt',
        'ruby',
        's',
        'samp',
        'section',
        'select',
        'small',
        'source',
        'span',
        'strong',
        'sub',
        'summary',
        'sup',
        'table',
        'tbody',
        'td',
        'textarea',
        'tfoot',
        'th',
        'thead',
        'time',
        'tr',
        'track',
        'u',
        'ul',
        'var',
        'video',
        'wbr',
    ]
    // public findacnhor = /<a[\s]+([^>]+)>((?:.(?!\<\/a\>))*.)<\/a>/g;
    public options ={
        lowerCaseTagName: false,  // convert tag name to lower case (hurt performance heavily)
        comment: false,         // retrieve comments (hurt performance slightly)
        blockTextElements: {
          script: true,	// keep text content when parsing
          noscript: true,	// keep text content when parsing
          style: true,		// keep text content when parsing
          pre: true			// keep text content when parsing
        }
      };
    constructor(private auth:AuthService,private zone: NgZone) {
        console.log(this);
        this.auth.detailedContent.subscribe(
            (tabsContent) => {
              this.zone.run(() => {
                this.htmlcontent = tabsContent;
                this.makehtmlElements(tabsContent);
              });
            }
          );
        
    }
    ngOnInit() {
        // console.log(this.htmlcontent);
        this.makehtmlElements(this.htmlcontent);
        // console.log(this.htmlelements);
    }
    makehtmlElements(htmlelements){
        if(htmlelements){
                var htmlele=[];
        htmlelements.map(item => {
            const ele = parse.parse(item,this.options);
            if (item.indexOf('<iframe') === -1) {
            ele.childNodes.map(item=>{
                htmlele.push(item);
            });
        }
        });
        this.htmlelements = htmlele;      
        }

    }
    openWindow(e){
        console.log(e);
    }
    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.htmlelements =[];
    }

}