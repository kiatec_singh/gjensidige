import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StorageService } from '../../providers/storageservice/storageservice';
import { AuthService } from '../../providers/authenticationservice/auth.service';


@Component({
    selector: 'cookie-viewer',
    templateUrl: 'cookie.html'
})
export class CookieComponent {


private agreed;
    constructor(private ref: ChangeDetectorRef, private storage: StorageService, private auth:AuthService ) {
           this.storage.get('acceptcookie').then(
            agreed => {
                this.agreed = agreed;
                console.log("from cokkie"+agreed);
                this.auth.setagreed(agreed);
            });
    }

    ngOnInit() {
        // Tracking 
    
    }

    setData() {


    }
    acceptCookie(){
        this.agreed = true;
    console.log("Accepte");
    this.ref.detectChanges();
    this.auth.setagreed(true);
    this.storage.set('acceptcookie',this.agreed);

    }
}


