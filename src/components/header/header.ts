import { Component, Input, ChangeDetectorRef, ViewChild, ViewRef } from '@angular/core';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { NavController, ViewController, Navbar } from 'ionic-angular';
import { Events, Platform } from 'ionic-angular';
import { NotificationsPage } from '../../pages/notifications/notifications.page';
import { HelperSerice } from '../../providers/utils/helperService';
import { EventLoggerProvider } from '../../providers/firebase/eventlogger';


@Component({
    selector: 'header-viewer',
    templateUrl: 'header.html'
    
})
export class HeaderComponent {

    private baseUrl = "https://connect.carex.dk/";
    private appconfig;
    private logo;

    @Input() hidemenu: any;
    @Input() showback: any;
    @Input() showhome: any;
    @Input() pagetitle: string;
    @Input() isScrolledvalue: any;
    @Input() topic: string;
    public testscroll = false;
    public toogleback = true;
    public navbarlist: any;
    @ViewChild(Navbar) navBar: Navbar;
    public notifycountvalue;

    constructor(public navCtrl: NavController, private ref: ChangeDetectorRef, private helperService: HelperSerice,private logprovider:EventLoggerProvider, private events: Events, private auth: AuthService) {
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                if (appconfig && appconfig.environment) {
                    this.appconfig = appconfig;
                    this.baseUrl = appconfig.environment.picturesUrl;
                    this.logo = appconfig.configuration.headerlogo;
                }
            }
        );
        this.auth.headerpagetopicTitle.subscribe((topic) => {
            this.topic = topic;
        });
        events.subscribe('scroll', (scroll) => {
            this.testscroll = scroll;
            //  scroll === true ? this.viewCtrl.showBackButton(true) : this.viewCtrl.showBackButton(false)
            this.ref.detectChanges();
        });
        this.testscroll = this.testscroll ? this.testscroll : false;
        //this.testscroll === true ? this.viewCtrl.showBackButton(true) : this.viewCtrl.showBackButton(false);
    }
    ngOnDestroy(): void {
        this.events.unsubscribe('scroll');
    }

    ngOnInit() {
        setTimeout(() => {
            this.navBar.backButtonClick = () => {
                this.navCtrl.pop();
                this.events.publish('scroll', false);
                this.ref.detectChanges();
            };
        }, 500);

        this.auth.notifycount.subscribe(
            (notifycountvalue) => {
                    this.notifycountvalue = notifycountvalue;
                    if (!this.ref['destroyed']) {
                        this.ref.detectChanges();
                    }
            });
    }

    gotoBack() {
        this.navCtrl.pop();
    }
    gotoHome() {
        this.events.unsubscribe('scroll');
        this.ref.detach();
        this.helperService.home();
    }
    gotoNotification() {
        if (this.appconfig.environment.trackanalytics === true) {
            this.logprovider.logpageView('Notifications');
          }
        this.navCtrl.setRoot(NotificationsPage, { 'resultData': '' });
        this.navCtrl.popToRoot();
    }
}
