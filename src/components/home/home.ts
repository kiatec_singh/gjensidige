import { Component, ViewChild, ChangeDetectorRef, NgZone, Renderer2 } from '@angular/core';
import { NavParams, NavController, Content, Navbar } from 'ionic-angular';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { pageData } from '../../models/pagedata';
import { StorageService } from '../../providers/storageservice/storageservice';
import { ListdetailsPage } from '../../pages/listdetails/listdetails.page';
import { AlldetailsPage } from '../../pages/alldetails/alldetails.page';
import { environment } from '../../environments/environment';
import { StatusBar } from '@ionic-native/status-bar';
import { ThemeChangeService } from '../../providers/restservice/themechange.service';
import { EventLoggerProvider } from '../../providers/firebase/eventlogger';
import { Network } from '@ionic-native/network';
import { AlertService } from '../../providers/utils/alertService';
import { Platform } from 'ionic-angular/platform/platform';
import { SmartsearchPage } from '../../pages/smartsearch/smartsearch.page';
import { ApplicationLoader } from '../../providers/utils/applicationloader';
import { FcmProvider } from '../../providers/firebase/firebasemessaging';
import { Appinfo } from '../../providers/utils/appinfo';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';
import { HelperSerice } from '../../providers/utils/helperService';
import { ToastService } from '../../providers/utils/toastService';


declare let FontAwesome: any;

@Component({
  selector: 'home-viewer',
  templateUrl: 'home.html'
})
export class HomeComponent {

  public pagedata: any;
  private homedata: any;
  public navdata: any;
  public logourl: string;
  public currentpageIndex;
  public pagemenuButtons: any = [];
  private loading;
  private pagedataModel;
  private userinfo: any;
  private isLoading = false;
  private appconfig;
  private baseUrl;
  private bg_image;
  private positionY = 0;
  private imageUrl: any;
  private authid;
  private detailsdata: any;
  private slctdItem: any;
  private tabs;
  private tabsgroup;
  private webtabsContent: any;
  private webtabstoDisplay: any;
  private appinfo: any;
  public homecontent: any;
  public websmartsearchContent: any;
  public websearchdtlContent: any;
  public webstressContent: any;
  public webcontactContent: any;
  public spinner = false;
  public themeid: any;
  public webquestionarieContent: any;
  public webhistroryquestionarieContent: any;
  private selectedlanguage: any;



  @ViewChild('content') content: Content;
  @ViewChild(Navbar) navbar: Navbar;
  @ViewChild('imageContainer') imageContainer;
  @ViewChild('maincontent') maincontent;
  @ViewChild('image') image;
  constructor(public navCtrl: NavController,
    private fcmProvider: FcmProvider, private rendere: Renderer2, private lang: ChangelanguageService, private toastService: ToastService,
    private ref: ChangeDetectorRef, private themechangeService: ThemeChangeService, private appinfoservice: Appinfo,
    private platform: Platform, private logprovider: EventLoggerProvider, private helperservice: HelperSerice,
    public statusbar: StatusBar, public auth: AuthService, private storageService: StorageService,
    public navParams: NavParams, private baserestService: BaseRestService) {
    this.baseUrl = environment.contentUrl ? environment.contentUrl : 'https://connect.carex.dk';
    this.auth.user.subscribe(
      (userinfo) => {
        this.userinfo = userinfo;
      }
    );
    this.auth.spinner.subscribe(
      (spinner: any) => {
        this.spinner = spinner;
      }
    );
    this.auth.detailedContent.subscribe(
      (webtabsContent: any) => {
        this.webtabsContent = webtabsContent;
      }
    );
    this.auth.websmartsearchContent.subscribe(
      (websmartsearchContent: any) => {
        this.websmartsearchContent = websmartsearchContent;
      }
    );
    this.auth.webstressContent.subscribe(
      (webstressContent: any) => {
        this.webstressContent = webstressContent;
      }
    );
    this.auth.webcontactContent.subscribe(
      (webcontactContent: any) => {
        this.webcontactContent = webcontactContent;
      }
    );
    this.auth.webquestionarieContent.subscribe(
      (webquestionarieContent: any) => {
        this.webquestionarieContent = webquestionarieContent;
      }
    );
    this.auth.websearchdtlContent.subscribe(
      (websearchdtlContent: any) => {
        this.websearchdtlContent = websearchdtlContent;
      }
    );
    this.auth.webtabstoDisplay.subscribe(
      (webtabstoDisplay: any) => {
        this.webtabstoDisplay = webtabstoDisplay;
      }
    );
    this.auth.appconfig.subscribe(
      (appconfig: any) => {
        this.appconfig = appconfig;
      }
    );
    this.auth.webhistroryquestionarieContent.subscribe(
      (webhistroryquestionarieContent: any) => {
        this.webhistroryquestionarieContent = webhistroryquestionarieContent;
      });
    this.lang.selectedlanguage.subscribe(
      (selectedlanguage: any) => {
        if (selectedlanguage) {
          this.selectedlanguage = selectedlanguage;
          this.homecontent = selectedlanguage.home.paragraphs;
        }
      }
    );

  }
  ngOnInit(): void {
    this.isLoading = true;
    this.auth.setSpinner(true);
    if (this.appconfig && this.appconfig.configuration) {
      if (!this.homedata) {
        this.baserestService.getCustomerData(this.userinfo.id).then(
          homedata => { this.homedata = homedata; this.setData(); },
          error => { this.isLoading = false }
        )
      }
    }
    if (!this.appconfig || !this.appconfig.configuration) {
      this.baserestService.getappConfig().then(
        (appconfig: any) => {
          this.appconfig = appconfig;
          this.auth.setappconfig(appconfig)
          this.currentpageIndex = this.navParams.get('itemIndex');
          if (!this.homedata) {
            this.baserestService.getCustomerData(this.userinfo.id).then(
              homedata => { this.homedata = homedata; this.setData(); },
              error => { this.isLoading = false }
            )
          }
        });
    }
  }

  createIcon() {
    if (this.appconfig.configuration.icons) {
      var customicon: any;
      for (let i in this.appconfig.configuration.icons) {
        customicon = {
          prefix: '' + this.appconfig.configuration.icons[i].prefix,
          iconName: '' + this.appconfig.configuration.icons[i].iconName,
          icon: this.appconfig.configuration.icons[i].icon,
        }
        if (FontAwesome) {
          FontAwesome.library.add(customicon);
        }
      }
    }
    this.isLoading = false;
    this.auth.setSpinner(false);
    this.notifications();
    this.logevents();
  }

  setData() {
    if (this.homedata && this.homedata.result.mine_services && !this.homedata.brand) {
      this.loadtheme();
    }
    if (this.homedata && this.homedata.result.mine_services && this.homedata.brand) {
      if (this.homedata.brand !== this.appconfig.configuration.appversion.organisationid) {
        this.changeTheme();
      }
      else {
        this.loadtheme();
      }
    }
  }
  loadtheme() {
    this.imageUrl = this.appconfig.environment.picturesUrl + this.appconfig.configuration.background_img;
    this.pagemenuButtons = this.homedata.result.mine_services.submenu;
    this.auth.setpagemenuButtons(this.pagemenuButtons);
    this.auth.setHomedata(this.homedata.result.mine_services.submenu);
    this.pagedataModel = new pageData(this.homedata);
    this.auth.setauthorisation(this.pagedataModel.pagedata.autorisation);
    this.appinfoservice.setusermenuObject(this.pagedataModel.pagedata);
    this.createIcon();
    if (!this.ref['destroyed']) {
      this.ref.detectChanges();
    }
  }
  changeTheme() {
    this.themeid = this.homedata.brand;
    this.storageService.set('themeid', this.homedata.brand);
    this.baserestService.getbrandConfigbyId(this.homedata.brand).then(
      (appconfig: any) => {
        this.imageUrl = appconfig.environment.picturesUrl + appconfig.configuration.background_img;
        this.pagemenuButtons = this.homedata.result.mine_services.submenu;
        this.auth.setpagemenuButtons(this.pagemenuButtons);
        this.auth.setHomedata(this.homedata.result.mine_services.submenu);
        this.pagedataModel = new pageData(this.homedata);
        this.auth.setauthorisation(this.pagedataModel.pagedata.autorisation);
        this.appinfoservice.setusermenuObject(this.pagedataModel.pagedata);
        this.statusbar.backgroundColorByHexString(appconfig.configuration.primary);
        if (appconfig.configuration.statusbartext == 'light') {
          this.statusbar.styleLightContent();
        }
        if (appconfig.configuration.statusbartext == 'dark') {
          this.statusbar.styleDefault();
        }
        this.auth.setappconfig(appconfig)
        this.themechangeService.changetoDynamicTheme(appconfig);
        this.appconfig = appconfig;
        this.createIcon();
        this.isLoading = false;
        if (!this.ref['destroyed']) {
          this.ref.detectChanges();
        }
      },
      (error) => {
        this.isLoading = false;
        let errormsg = error && error.error ? error.error : '';
        this.toastService.errorblock(errormsg, '');
        this.loadtheme();
      });
  }

  scrollComplete(e) {
    if (this.platform.is('cordova') === true) {
      if (e.scrollTop > 0) {
        let translateAmt = e.scrollTop / 2;
        this.rendere.setStyle(this.imageContainer.nativeElement, 'transform', 'translate3d(0px,' + translateAmt + 'px,0px)');
        this.rendere.setStyle(this.maincontent.nativeElement, 'transform', 'translate3d(0px,0px,0px)');
        this.ref.detectChanges();
      }
      else {
        this.rendere.setStyle(this.imageContainer.nativeElement, 'transform', 'translate3d(0px,0px,0px)');
        this.rendere.setStyle(this.maincontent.nativeElement, 'transform', 'translate3d(0px,0px,0px)');
        this.ref.detectChanges();
      }
    }
  }
  scrolling(e) {
    if (this.platform.is('cordova') === true) {
      if (e.scrollTop > 0) {
        let translateAmt = e.scrollTop / 2;
        this.rendere.setStyle(this.imageContainer.nativeElement, 'transform', 'translate3d(0px,' + translateAmt + 'px,0px)');
        this.rendere.setStyle(this.maincontent.nativeElement, 'transform', 'translate3d(0px,0px,0px)');
        this.ref.detectChanges();

      }
      else {
        this.rendere.setStyle(this.imageContainer.nativeElement, 'transform', 'translate3d(0px,0px,0px)');
        this.rendere.setStyle(this.maincontent.nativeElement, 'transform', 'translate3d(0px,0px,0px)');
        this.ref.detectChanges();
      }
    }
  }
  detailsPage(slctdItem) {
    this.isLoading = true;
    this.ref.detectChanges();
    this.slctdItem = slctdItem;
    let actionparameterkey = '';
    let actionparameterkeyId = '';
    if (this.slctdItem.click_action_parameters && this.slctdItem.click_action_parameters.length) {
      for (let i in this.slctdItem.click_action_parameters[0]) {
        actionparameterkey = i[0];
        actionparameterkeyId = this.slctdItem.click_action_parameters[0][i]
      }
    }
    if (this.slctdItem.click_action) {
      this.baserestService.getdetails(this.slctdItem.click_action, this.slctdItem.skema_uuid, this.pagedataModel.pagedata.autorisation).then(
        detailsdata => {
          this.detailsdata = detailsdata;
          this.naviDetails();
        },
        error => { this.isLoading = false }
      )
    }
    if (this.slctdItem.elementtype && this.slctdItem.elementtype === "group_buttons") {
      this.isLoading = false;
      if (this.appconfig.environment.trackanalytics === true) {
        this.logprovider.logEvent(this.slctdItem.funktionsnavn);
      }
      this.helperservice.setlistdetailsmenu(this.slctdItem.submenu);
      this.navCtrl.push(ListdetailsPage, { 'selectedItem': this.slctdItem, 'submenu_text': this.slctdItem.submenu_text, 'pagetitle': this.slctdItem.funktionsnavn, 'heading': null }, { animate: true, direction: 'forward' });

    }
    if (this.slctdItem.elementtype && this.slctdItem.elementtype === "group_tabs") {
      let tabsContent: any;
      this.tabs = this.slctdItem.submenu;
      let tabstoDisplay = [];
      let action = this.tabs[0].click_action;
      let id = this.tabs[0].skema_uuid;
      if (this.appconfig.environment.trackanalytics === true) {
        this.logprovider.logEvent(this.tabs[0].funktionsnavn);
      }

      for (let item in this.tabs) {
        if (this.tabs[item].click_action) {
          tabstoDisplay.push({
            'name': this.tabs[item].funktionsnavn,
            'icon': this.tabs[item].ikon,
            'id': this.tabs[item].skema_uuid,
            'type': this.tabs[item].elementtype,
            'action': this.tabs[item].click_action,
            'click_action_parameters': this.tabs[item].click_action_parameters ? this.tabs[item].click_action_parameters : null
          });
        }
        else {
          tabstoDisplay.push({
            'name': this.tabs[item].funktionsnavn,
            'icon': this.tabs[item].ikon,
            'id': this.tabs[item].skema_uuid,
            'type': this.tabs[item].elementtype,
          });
        }
      } this.baserestService.getdetails(action, id, this.pagedataModel.pagedata.autorisation).then(
        detailsdata => {
          this.tabsgroup = detailsdata;
          this.showdetailsPage(tabstoDisplay);
        },
        error => { this.isLoading = false }
      )
    }
  }
  showdetailsPage(tabstoDisplay) {
    this.isLoading = false;
    this.auth.setdetailContent(this.tabsgroup.indhold.indhold);
    this.navCtrl.push(AlldetailsPage, { 'tabstoDisplay': tabstoDisplay, 'pagetitle': this.slctdItem.funktionsnavn });
  }
  naviDetails() {
    let tabsContent = [];
    let tabstoDisplay = [];
    if (this.slctdItem.click_action) {
      if (this.detailsdata && this.detailsdata.indhold) {
        this.isLoading = false;
        tabstoDisplay.push(this.detailsdata.indhold.tab_titel)
      }
      if (this.detailsdata && this.detailsdata.group_tabs) {
        this.isLoading = false;
        tabstoDisplay.push(this.detailsdata.indhold.tab_titel)
      }
      if (this.slctdItem.click_action === "objektListSmartSearch") {
        this.isLoading = false;
        this.navCtrl.push(SmartsearchPage, { 'pagetitle': 'Smart søg', 'smartsearchData': this.detailsdata });
      }
      else {
        this.isLoading = false;
        if (this.detailsdata && this.detailsdata.indhold && this.detailsdata.indhold.indhold) {
          this.auth.setdetailContent(this.detailsdata.indhold.indhold);
          this.navCtrl.push(AlldetailsPage, { 'tabstoDisplay': tabstoDisplay, 'pagetitle': this.slctdItem.funktionsnavn });

        }
      }
    }
  }
  notifications() {
    this.fcmProvider.subscribforNotifications();
  }
  logevents() {
    if (this.appconfig.environment.trackanalytics === true) {
      this.logprovider.logpageView("Homepage");
    }
  }
}
