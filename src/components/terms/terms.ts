import { Component, Input } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';
import {AuthService} from '../../providers/authenticationservice/auth.service';

@Component({
  selector: 'trems-viewer',
  templateUrl: 'terms.html'
})


export class TermsComponent {

private title:string;
private termsContent:any;
private contacttextContent:any;
private contactForm:any;
  constructor(private navCtrl: NavController, private viewCtrl: ViewController,private params: NavParams) {
   
    this.termsContent =  this.params.get('termsdata');
    this.contactForm = this.params.get('formdata');
    this.contacttextContent = this.params.get('contacttextContent');

  }
  ngOnInit() {
  }
  closemodal(){
    this.viewCtrl.dismiss();

  }
}