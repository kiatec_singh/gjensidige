import { Component, ViewChild, Input } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { ChangepasswordPage } from '../../pages/changepassword/changepassword';
import { Keyboard } from '@ionic-native/keyboard';
import { Vibration } from '@ionic-native/vibration';
import { Platform } from 'ionic-angular';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';
import * as  moment from 'moment';

@Component({
    selector: 'otp-viewer',
    templateUrl: 'otp.html'
})
export class OtpComponent {


    private loginForm: FormGroup;
    private userid;
    private otpval1;
    private otpval2;
    private otpval3;
    private otpval4;
    @ViewChild("otp1") otp1_input;
    @ViewChild("otp2") otp2_input;
    @ViewChild("otp3") otp3_input;
    @ViewChild("otp4") otp4_input;
    private username;
    private error = false;
    private isLoading=false;
    private otpcontent;
    private selectedlanguage;
    private interval;
    private timerDisplay;


    constructor(private fb: FormBuilder, private keyboard: Keyboard, public navParams: NavParams,private navCtrl: NavController, private platform: Platform, private lang:ChangelanguageService,
        private auth: AuthService, private baserestService: BaseRestService, private vibration: Vibration) {
        this.loginForm = this.fb.group({
            otp1: ['', Validators.required],
            otp2: ['', Validators.required],
            otp3: ['', Validators.required],
            otp4: ['', Validators.required]
        });
          platform.resume.subscribe(
            (result) => {
                setTimeout(() => {
                    this.showKeyboard();
                }, 500);
            }
          );
          this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                    this.selectedlanguage = selectedlanguage;
            }
        );
    }

    ngOnInit() {
        this.username = this.navParams.get('username');
        this.otpcontent = this.selectedlanguage.otp;
        this.startTimer();
        setTimeout(() => {
            this.otp1_input.setFocus();
        }, 700);

    }  
    startTimer() {
        var start = moment(this.otpcontent.duration, "m:ss");
        var seconds = start.minutes() * 60;
        this.isLoading = false;
        this.interval = setInterval(() => {
          this.timerDisplay = start.subtract(1, "second").format("m:ss");
          seconds--;
          if (seconds === 0) clearInterval(this.interval);
        }, 1000);
      }
    
      resendOTP(){
        this.isLoading = true;
        this.error = false;
        this.baserestService.OTP(this.username).then(
            (success) => {
                this.isLoading = false;
                this.startTimer();
            },
            error => {
                console.log(error);
                // noemailerror
                this.error = true;
                this.isLoading = false;

            }
        )
      }
    gotochangePasword() {
        this.isLoading = true;
        this.otpval1 = this.loginForm.value.otp1;
        this.otpval2 = this.loginForm.value.otp2;
        this.otpval3 = this.loginForm.value.otp3;
        this.otpval4 = this.loginForm.value.otp4;
        let otp = '' + this.otpval1 + this.otpval2 + this.otpval3 + this.otpval4;
        if (this.loginForm.valid) {
            this.baserestService.verifyOTP(this.username, otp).then(
                (userid) => { this.error = false; 
                    this.userid = userid;
                     this.setuserData(); 
                    },
                error => {
                    this.error = true;
                    this.otp1_input.setFocus();
                    this.otpval1 = null; this.loginForm.value.otp1 = null;
                    this.otpval2 = null; this.loginForm.value.otp2 = null;
                    this.otpval3 = null; this.loginForm.value.otp3 = null;
                    this.otpval4 = null; this.loginForm.value.otp4 = null;
                    this.isLoading = false;
                    this.vibration.vibrate(500); console.log(error)
                }
            ), this
        }

    }
    next1(event) {
        this.error = false;
        if (event.keycode == 46 || event.key == "Backspace") {
            //this.removeFocus(event.currentTarget.id);
            this.otpval1 = null; this.loginForm.value.otp1 = null;
        }
        else {
            if (this.platform.is('android')) {
                this.otpval1 = event.target.value;
            }
            else {
                this.otpval1 = event.key;
            }

            this.otp2_input.setFocus();
        }

    }
    next2(event) {
        this.error = false;
        if (event.keycode == 46 || event.key == "Backspace") {
            this.otpval1 = null; this.loginForm.value.otp1 = null;
            this.otp1_input.setFocus();
        }
        else {
            if (this.platform.is('android')) {
                this.otpval2 = event.target.value;
            }
            else {
                this.otpval2 = event.key;
            }
            this.otp3_input.setFocus();
        }

    }
    next3(event) {
        this.error = false;
        if (event.keycode == 46 || event.key == "Backspace") {
            this.otpval2 = null; this.loginForm.value.otp2 = null;
            this.otp2_input.setFocus();
        }
        else {
            if (this.platform.is('android')) {
                this.otpval3 = event.target.value;
            }
            else {
                this.otpval3 = event.key;
            }

            this.otp4_input.setFocus();
        }

    }
    next4(event) {
        this.error = false;
        if (event.keycode == 46 || event.key == "Backspace") {
            this.otpval3 = null; this.loginForm.value.otp3 = null;
            this.otp3_input.setFocus();
        }
        else {
            if (this.platform.is('android')) {
                this.otpval4 = event.target.value;
            }
            else {
                this.otpval4 = event.key;
            }
            if (this.otpval1 && this.otpval2 && this.otpval3 && this.otpval4) {
                this.gotochangePasword();
            }

        }

    }
    setuserData() {
        this.isLoading = false;
        this.navCtrl.push(ChangepasswordPage,{"userid":this.userid});
    }

    showKeyboard() {
        this.otp1_input.setFocus();
        this.otpval1 = null; this.loginForm.value.otp1 = null;
        this.otpval2 = null; this.loginForm.value.otp2 = null;
        this.otpval3 = null; this.loginForm.value.otp3 = null;
        this.otpval4 = null; this.loginForm.value.otp4 = null;

    }
}