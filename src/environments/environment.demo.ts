export const environment = {
  production: false,
  baseURL: "https://demo-api.carex.dk/api/endpoints/api_services.php",
  contentUrl: "https://demo-workplace.carex.dk/",
  apiUrl: "https://demo-api.carex.dk/api/endpoints/api_services.php",
  loginservicesUrl: "https://demo-idp.carex.dk/endpoints/login_services.php",
  idpservicesUrl: "https://demo-idp.carex.dk/endpoints/idp_service.php",
  idpservicesUrl_v2: "https://demo-idp.carex.dk/v2/endpoints/idp_service.php",
  nemidloginUrl: "https://demo-nemid.carex.dk/nemidlogon.php",
  nemidvalidateUrl: "https://demo-nemid.carex.dk/validatecpr.php",
  webUrl: 'https://demo-workplace.carex.dk',
  picturesUrl: "https://demo-admin.carex.dk/pictures/",
  bilagUrl: "https://demo-admin.carex.dk/temp/",
  version: '1.3.66',
  fcmwebSenderId:'116581142159',
  application_version_uuid:'8f7a974c-6ee3-4b81-acf2-376fb503e640',
  envi: 'demo'
};