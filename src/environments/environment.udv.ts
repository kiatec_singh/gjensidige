export const environment = {
  production: false,
  baseURL :"https://udv-api.carex.dk/api/endpoints/api_services.php",
  contentUrl: "https://udv-workplace.carex.dk/",
  apiUrl: "https://udv-api.carex.dk/api/endpoints/api_services.php",
  loginservicesUrl: "https://udv-idp.carex.dk/endpoints/login_services.php",
  idpservicesUrl:"https://udv-idp.carex.dk/endpoints/idp_service.php",
  idpservicesUrl_v2: "https://udv-idp.carex.dk/v2/endpoints/idp_service.php",
  nemidloginUrl: "https://udv-nemid.carex.dk/nemidlogon.php",
  nemidvalidateUrl: "https://udv-nemid.carex.dk/validatecpr.php",
  webUrl:'https://udv-workplace.carex.dk',
  picturesUrl: "https://udv-admin.carex.dk/pictures/",
  bilagUrl: "https://udv-admin.carex.dk/temp/",
  version: '1.3.66',
  fcmwebSenderId:'116581142159',
  application_version_uuid:'8f7a974c-6ee3-4b81-acf2-376fb503e640',
  envi: 'udv'
}; 