export const environment = {
  production: false,
  baseURL :"https://qa-api.carex.dk/api/endpoints/api_services.php",
  contentUrl: "https://qa-workplace.carex.dk",
  apiUrl: "https://qa-api.carex.dk/api/endpoints/api_services.php",
  loginservicesUrl: "https://qa-idp.carex.dk/endpoints/login_services.php",
  idpservicesUrl:"https://qa-idp.carex.dk/endpoints/idp_service.php",
  idpservicesUrl_v2: "https://qa-idp.carex.dk/v2/endpoints/idp_service.php",
  nemidloginUrl: "https://qa-nemid.carex.dk/nemidlogon.php",
  nemidvalidateUrl: "https://qa-nemid.carex.dk/validatecpr.php",
  webUrl:'https://qa-workplace.carex.dk',
  picturesUrl: "https://qa-admin.carex.dk/pictures/",
  bilagUrl: "https://qa-admin.carex.dk/temp/",
  version: '1.3.66',
  fcmwebSenderId:'116581142159',
  application_version_uuid:'8f7a974c-6ee3-4b81-acf2-376fb503e640',
  envi: 'qa'
};