//DEFAULT
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, IonicPageModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { FCM } from '@ionic-native/fcm';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Keyboard } from '@ionic-native/keyboard';
import { Market } from '@ionic-native/market';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { Vibration } from '@ionic-native/vibration';
import { Toast } from '@ionic-native/toast';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Network } from '@ionic-native/network';
import { AnalyticsFirebase } from '@ionic-native/analytics-firebase';
import { YoutubePlayerModule } from 'ngx-youtube-player';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { Push } from '@ionic-native/push';
import { ProgressBarModule } from "angular-progress-bar";
import { InAppBrowser } from '@ionic-native/in-app-browser';



//ANGULAR
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home.page';
import { ListPage } from '../pages/list/list';
import { MenuPage } from '../pages/menu/menu.page';
import { DetailsPage } from '../pages/details/details.page';
import { SettingsPage } from '../pages/settings/settings.page';
import { ProfilePage } from '../pages/profile/profile.page';
import { NotificationsPage } from '../pages/notifications/notifications.page';
import { LoginPage } from '../pages/login/login.page';
import { WelcomePage } from '../pages/welcome/welcome.page';
import { NemidPage } from '../pages/nemid/nemid.page';
import { CPRPage } from '../pages/cpr/cpr.page';
import { TermsconditionPage } from '../pages/termsconditions/termsconditions.page';
import { SearchDetailsPage } from '../pages/searchdetails/searchdetails.page';
import { OtherRelationsPage } from '../pages/otherrelations/otherrelations.page';
import { CustomanchorPage } from '../pages/customanchor/customanchor.page';
import { ChangepasswordPage } from '../pages/changepassword/changepassword';
import { OtpPage } from '../pages/otp/otp';
import { UsernamePage } from '../pages/username/username';
import { QuestionaireComponent } from '../components/questionaire/questionaire';
import { ContactComponent } from '../components/contact/contact';
import { HeaderLogoComponent } from '../components/header-logo/header-logo';
import { IdverifyComponent } from '../components/idverify/idverify';
import { IdverifyPage } from '../pages/idverify/idverify.page';
import { ListdetailsPage } from '../pages/listdetails/listdetails.page';
import { TabsdetailsPage } from '../pages/tabsdetails/tabsdetails.page';
import { ContentPage } from '../pages/content/content.page';
import { AlldetailsPage } from '../pages/alldetails/alldetails.page';
import { ChartPage } from '../pages/chart/chart.page';
import { BackPage } from '../pages/back/back.page';
import { ConfigurePage } from '../pages/configure/configure.page';
import { FooterPage } from '../pages/footer/footer.page';
import { SmartsearchPage } from '../pages/smartsearch/smartsearch.page';
import { contactPage } from '../pages/contact/contact.page';
import { StressPage } from '../pages/stress/stress.page';
import { StressmodalPage } from '../pages/stressmodalpage/stressmodal.page';
import { QuestionairePage } from '../pages/questionaire/questionaire.page';
import { questionairehistoryPage } from '../pages/questionairehistory/questionairehistory.page';
import { questionaireresultPage } from '../pages/questionaireresult/questionaireresult.page';
import { ChangelanguagePage } from '../pages/changelanguage/changelanguage.page';

//MODALS
// import {ModalChart} from '../components/modals/modal-chart/modal-chart'



//PROVIDERS
import { BaseRestService } from '../providers/restservice/base.rest.service';
import { StorageService } from '../providers/storageservice/storageservice';
import { WindowRef } from '../providers/windowservice/windowservice';
import { AuthService } from '../providers/authenticationservice/auth.service';
import { SafePipe } from '../providers/directory/safepipe';
import { ConfigurationService } from '../providers/utils/configservices';
import { PasswordValidation } from '../providers/validators/password-validator';
import { ThemeChangeService } from '../providers/restservice/themechange.service';
import { ElasticHeaderDirective } from '../providers/directory/elastic-header';
import { AlertService } from '../providers/utils/alertService';
import { MenugeneratorService } from '../providers/utils/menugenerator';
import { EventLoggerProvider } from '../providers/firebase/eventlogger';
import { Appinfo } from '../providers/utils/appinfo';
import { ApplicationLoader } from '../providers/utils/applicationloader';
import { enviSerice } from '../providers/utils/enviService';
import { FcmProvider } from '../providers/firebase/firebasemessaging';
import { Device } from '@ionic-native/device';
import { PushNotificationService } from '../providers/firebase/pushnotificationservice';
import { QuestionaireService } from '../providers/authenticationservice/questionaire.service';
import { deeplinkingService } from '../providers/utils/deeplinkingService';
import { HelperSerice } from '../providers/utils/helperService';
import { ChangelanguageService } from '../providers/utils/changelanguageservice';
import { ToastService } from '../providers/utils/toastService';
// import {AppRoutingModule} from '../app/app.router.module';


//IONIC COMPONENTS
import { MenuComponent } from '../components/menu/menu';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SettingsComponent } from '../components/settings/settings';
import { ProfileComponent } from '../components/profile/profile';
import { NotificationsComponent } from '../components/notifications/notifications';
import { HomeComponent } from '../components/home/home';
import { NemidComponent } from '../components/nemid/nemid';
import { LoginComponent } from '../components/login/login';
import { WelcomeComponent } from '../components/welcome/welcome';
import { TermsconditionsComponent } from '../components/termsconditions/termsconditions';
import { HeaderComponent } from '../components/header/header';
import { SearchDetailsComponent } from '../components/searchdetails/searchdetails';
import { OtherrelationsComponent } from '../components/otherrelations/otherrelations';
import { CustomanchorComponent } from '../components/customanchor/customanchor';
import { PopoverIonicdeploy } from '../components/ionicpopover/ionicpopover';
import { DropDownPopOver } from '../components/dropdownpopover/dropdownpopover';
import { changepasswordComponent } from '../components/changepassword/changepassword';
import { UsernameComponent } from '../components/username/username';
import { OtpComponent } from '../components/otp/otp';
import { CPRComponent } from '../components/cpr/cpr';
import { ListdetailsComponent } from '../components/listdetails/listdetails';
import { tabsdetailsComponent } from '../components/tabsdetails/tabsdetails';
import { ContentComponent } from '../components/content/content';
import { AlldetailsComponent } from "../components/alldetails/alldetails";
import { ChartComponent } from "../components/chart/chart";
import { BackComponent } from "../components/back/back";
import { ConfigureComponent } from "../components/configure/configure";
import { QuestionairenavigationComponent } from "../components/questionairenavigation/questionaire.navigation";
import { LeftmenuComponent } from "../components/leftmenu/leftmenu";
import { navigationbarComponent } from "../components/navigationbar/navigationbar";
import { FooterComponent } from '../components/footer/footer';
import { SmartsearchComponent } from '../components/smartsearch/smartsearch';
import { CookieComponent } from '../components/cookie/cookie';
import { VideoswipeComponent } from '../components/videoswipe/videoswipe';
import { changeenviComponent } from '../components/changeenvi/changeenvi';
import { TermsComponent } from '../components/terms/terms';
import { StressComponent } from '../components/stress/stress';
import { StressmodalComponent } from '../components/stressmodal/stressmodal';
import { QuestionairehistoryComponent } from '../components/questionairehistory/questionairehistory';
import { CharthistorymodalComponent } from '../components/charthistorymodal/charthistorymodal';
import { QuestionaireresultComponent } from '../components/questionaireresult/questionaire.result';
import { changelanguageComponent } from '../components/changelanguage/changelanguage';
import { CustomiframeComponent } from '../components/customiframe/customiframe';
import {  VimeoComponent } from '../components/vimeo/vimeo';
import { HtmlcontentComponent } from '../components/htmlcontent/htmlcontent';

const pagesDeclaration = [
  MenuPage,
  MyApp,
  HomePage,
  ListPage,
  DetailsPage,
  NotificationsPage,
  SettingsPage,
  ProfilePage,
  LoginPage,
  WelcomePage,
  NemidPage,
  SearchDetailsPage,
  TermsconditionPage,
  OtherRelationsPage,
  CustomanchorPage,
  UsernamePage,
  ChangepasswordPage,
  OtpPage,
  CPRPage,
  IdverifyPage,
  ListdetailsPage,
  TabsdetailsPage,
  ContentPage,
  AlldetailsPage,
  ChartPage,
  BackPage,
  ConfigurePage,
  FooterPage,
  SmartsearchPage,
  contactPage,
  StressPage,
  QuestionairePage,
  questionairehistoryPage,
  StressmodalPage,
  questionaireresultPage,
  ChangelanguagePage
];

const componentDeclaration = [
  MenuComponent,
  SettingsComponent,
  ProfileComponent,
  HomeComponent,
  NotificationsComponent,
  LoginComponent,
  WelcomeComponent,
  NemidComponent,
  TermsconditionsComponent,
  HeaderComponent,
  HeaderLogoComponent,
  SearchDetailsComponent,
  SafePipe,
  OtherrelationsComponent,
  CustomanchorComponent,
  PopoverIonicdeploy,
  DropDownPopOver,
  UsernameComponent,
  OtpComponent,
  changepasswordComponent,
  QuestionaireComponent,
  ContactComponent,
  CPRComponent,
  IdverifyComponent,
  ListdetailsComponent,
  tabsdetailsComponent,
  ContentComponent,
  AlldetailsComponent,
  ChartComponent,
  BackComponent,
  ConfigureComponent,
  QuestionairenavigationComponent,
  LeftmenuComponent,
  navigationbarComponent,
  FooterComponent,
  SmartsearchComponent,
  CookieComponent,
  VideoswipeComponent,
  changeenviComponent,
  TermsComponent,
  StressComponent,
  QuestionairehistoryComponent,
  StressmodalComponent,
  QuestionaireresultComponent,
  CharthistorymodalComponent,
  changelanguageComponent,
  CustomiframeComponent,
  HtmlcontentComponent,
   VimeoComponent

];

@NgModule({
  declarations: [
    ElasticHeaderDirective,
    ...pagesDeclaration,
    ...componentDeclaration
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    YoutubePlayerModule,
    BrowserAnimationsModule,
    Ng2GoogleChartsModule,
    ProgressBarModule,
    ToastrModule.forRoot({
      preventDuplicates: true
    }),
    // AppRoutingModule,
    IonicModule.forRoot(MyApp, {
      iconMode: 'ios',
      backButtonIcon: 'ios-arrow-back',
      backButtonText: '',
      platforms: {
        android: {
          scrollAssist: false,
          autoFocusAssist: false,
          scrollPadding: false
        },
        ios: {
          scrollAssist: true,
          scrollPadding: false
        }
      }
    }),
    // IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: 'sundheduniversDB',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    MenuPage,
    NotificationsPage,
    SettingsPage,
    ProfilePage,
    DetailsPage,
    LoginPage,
    WelcomePage,
    NemidPage,
    SearchDetailsPage,
    TermsconditionPage,
    OtherRelationsPage,
    PopoverIonicdeploy,
    UsernamePage,
    ChangepasswordPage,
    OtpPage,
    DropDownPopOver,
    CPRPage,
    IdverifyPage,
    ListdetailsPage,
    TabsdetailsPage,
    ContentPage,
    AlldetailsPage,
    ChartPage,
    ChartComponent,
    BackComponent,
    ConfigureComponent,
    ConfigurePage,
    navigationbarComponent,
    SmartsearchPage,
    changeenviComponent,
    contactPage,
    StressPage,
    StressmodalPage,
    ContactComponent,
    StressComponent,
    StressmodalComponent,
    CharthistorymodalComponent,
    TermsComponent,
    questionairehistoryPage,
    QuestionairePage,
    QuestionairehistoryComponent,
    QuestionaireresultComponent,
    questionaireresultPage,
    changelanguageComponent,
    ChangelanguagePage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    BaseRestService,
    AlertService,
    StorageService,
    ConfigurationService,
    HttpClient,
    Keyboard,
    Market,
    HttpClientModule,
    WindowRef,
    Vibration,
    PasswordValidation,
    AuthService,
    InAppBrowser,
    ScreenOrientation,
    ThemeChangeService,
    Network,
    Toast,
    MenugeneratorService,
    Appinfo,
    ApplicationLoader,
    AnalyticsFirebase,
    enviSerice,
    FCM,
    FcmProvider,
    deeplinkingService,
    PushNotificationService,
    Device,
    QuestionaireService,
    ToastrModule,
    HelperSerice,
    ChangelanguageService,
    Push,
    ToastService,
    EventLoggerProvider,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
