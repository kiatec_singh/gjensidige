module.exports = {
  includePaths: [
    'node_modules/ionic-angular/themes',
    'node_modules/ionicons/dist/scss',
    'node_modules/ionic-angular/fonts',
    'node_modules/@fortawesome/fontawesome-free/scss',
    '/node_modules/jqwidgets-scripts/jqwidgets/styles'
  ]
};